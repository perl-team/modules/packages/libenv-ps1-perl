libenv-ps1-perl (0.06-4) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + libenv-ps1-perl: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 13 Oct 2022 21:06:16 +0100

libenv-ps1-perl (0.06-3) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Ryan Niebur from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Laurent Baillet ]
  * fix lintian file-contains-trailing-whitespace warning

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Mon, 13 Jun 2022 21:37:30 +0100

libenv-ps1-perl (0.06-2.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Thu, 07 Jan 2021 14:01:00 +0100

libenv-ps1-perl (0.06-2) unstable; urgency=low

  * Team upload.

  [ Ryan Niebur ]
  * Email change: Ryan Niebur -> ryan@debian.org

  [ gregor herrmann ]
  * debian/control: Changed: (build-)depend on perl instead of perl-
    modules.
  * Remove alternative (build) dependencies that are already satisfied
    in oldstable.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 3.9.6.
  * Switch to source format "3.0 (quilt)".
  * Add explicit build dependency on libmodule-build-perl.
  * Update license stanzas in debian/copyright.
  * Bump debhelper compatibility level to 9.
  * Add a patch to fix spelling mistakes in the POD.

 -- gregor herrmann <gregoa@debian.org>  Tue, 09 Jun 2015 21:55:10 +0200

libenv-ps1-perl (0.06-1) unstable; urgency=low

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Ryan Niebur ]
  * New upstream release
  * Debian Policy 3.8.2
  * remove quilt patching, all are applied upstream
  * minimize d/rules
  * update d/copyright

 -- Ryan Niebur <ryanryan52@gmail.com>  Tue, 23 Jun 2009 23:25:59 -0700

libenv-ps1-perl (0.05-3) unstable; urgency=low

  * stop removing the .packlist in debian/rules, it's now causing a
    FTBFS (Closes: #531232)

 -- Ryan Niebur <ryanryan52@gmail.com>  Sun, 31 May 2009 00:01:51 -0700

libenv-ps1-perl (0.05-2) unstable; urgency=low

  * Take over for the Debian Perl Group on maintainer's request
    (http://lists.debian.org/debian-qa/2008/12/msg00091.html)
  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza); Homepage field (source stanza). Changed:
    Maintainer set to Debian Perl Group <pkg-perl-
    maintainers@lists.alioth.debian.org>
  * debian/watch: use dist-based URL.
  * add myself to uploaders
  * machine readable copyright format
  * debhelper 7
  * Debian Policy 3.8.1
  * add quilt patching
    - fix example
    - fix command substitution
    - fix when PS1 is HOME
  * fix whitespace in short description

 -- Ryan Niebur <ryanryan52@gmail.com>  Fri, 24 Apr 2009 22:35:36 -0700

libenv-ps1-perl (0.05-1) unstable; urgency=low

  * New upstream release.

 -- Marc 'HE' Brockschmidt <he@debian.org>  Thu, 25 Nov 2004 19:15:42 +0100

libenv-ps1-perl (0.04-1) unstable; urgency=low

  * New upstream release.

 -- Marc 'HE' Brockschmidt <he@debian.org>  Wed, 11 Aug 2004 17:46:12 +0200

libenv-ps1-perl (0.03-1) unstable; urgency=low

  * New upstream release.
  * Added debian/watch file.

 -- Marc 'HE' Brockschmidt <he@debian.org>  Thu,  6 May 2004 10:26:12 +0200

libenv-ps1-perl (0.02-1) unstable; urgency=low

  * Initial Release.

 -- Marc 'HE' Brockschmidt <he@debian.org>  Sun, 14 Mar 2004 17:30:44 +0100
